/**
 * lib/host.js - balancr
 * Licensed under GPLv3.
 * Copyright (C) 2015 Online Health Database.
 **/

"use strict";

var os = require('os-utils'),
    client = require('dgram').createSocket('udp4'),
    EventEmitter = require('events').EventEmitter

module.exports = function (server) {
    var exports = new EventEmitter(),
        loadSize = 0

    // send out regular updates to stay online
    setInterval(function () {
        // avoid negatives
        loadSize = loadSize < 0 ? 0 : loadSize

        // send over just the number
        var message = new Buffer(String(loadSize) + '\0')
        client.send(message, 0, message.length, 5253, server, function (err) {
            if (err) exports.emit('error', err)
        })
    }, 100)

    // attach a noop to bring the event into existence
    exports.on('error', function () {})

    // `push` to add a client to the stack
    exports.push = function () {
        loadSize += 1
    }

    // `pop` to remove a client from the stack
    exports.pop = function () {
        loadSize -= 1
    }

    // `update` to set the load size manually
    exports.update = function (size) {
      if (size === undefined) {
        os.cpuUsage(function (size) {
          loadSize = size
        })
      } else {
        loadSize = size
      }
    }

    return exports
}
