/**
 * lib/server.js - balancr
 * Licensed under GPLv3.
 * Copyright (C) 2015 Online Health Database.
 **/

"use strict";

var log = require('util').debuglog('balancr'),
    server = require('dgram').createSocket('udp4'),
    clone = function (obj) {
      return JSON.parse(JSON.stringify(obj))
    }

// startup a UDPv4 server to receive the load updates
server.bind(5253)

module.exports = function (hosts, latency) {
    var i, table = {}, check = function () {
      var bkp = clone(table)
      setTimeout(function () {
        for (var n in bkp) {
          if (bkp.hasOwnProperty(n) && bkp[n] === table[n]) {
            table[n] = Infinity
          }
        }

        check()
      }, latency || 301)
    }

    // allocate spots for load counters for
    // every host specified
    for (i = 0; i < hosts.length; i += 1) {
        table[hosts[i]] = 0
    }

    // wait for load updates
    server.on('message', function (data, info) {
        log('\n%s\n', JSON.stringify(table, null, 2))

        data = String(data).split('\0').filter(function (piece) {
            return !!piece
        })
        data = data[data.length - 1]

        if (data === 'gimme') {
            var msg = [Infinity,'1.1.1.1'], i

            for (i in table) {
              if (table.hasOwnProperty(i) && table[i] < msg[0]) {
                msg = [table[i], i]
              }
            }

            if (!isFinite(msg[0])) {
              msg = [-1, '1.1.1.1']
            }

            msg = new Buffer(msg[1])
            server.send(msg, 0, msg.length, info.port, info.address)
        } else if (hosts.indexOf(info.address) !== -1) {
            table[info.address] = parseFloat(data)
        }
    })

    // begin testing for offline hosts
    check()
}
