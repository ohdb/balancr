/**
 * lib/client.js - balancr
 * Licensed under GPLv3.
 * Copyright (C) 2015 Online Health Database.
 **/

"use strict";

module.exports = function (server, then) {
    var client = require('dgram').createSocket('udp4'),
        msg = new Buffer('gimme\0'),
        resolved = false,
        check = function () {
          if (!resolved) {
            client.send(msg, 0, msg.length, 5253, server)
            setTimeout(check, 301)
          }
        }

    client.on('message', function (data) {
        data = String(data).split('\0').filter(function (piece) {
          return piece
        })[0]

        if (data && data !== '1.1.1.1') {
          client.close()
          then(data)
        }
    })
}
