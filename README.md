# balancr

a load balancer for multi-threaded multi-server applications.

[![NPM](https://nodei.co/npm/balancr.png?downloads=true&downloadRank=true&stars=true)](https://nodei.co/npm/balancr/)

## Overview

*Install balancr using the standard way with npm: `npm install --save balancr`.*

Balancr works across three roles: the server, the hosts, and the client. Without
a load balancer balancing client loads, you would build your standard server and
clients where the server accepts all incoming connections and processes them the
way your application requires. With balancr, this process does not change. What
changes is you have to add a tiny bit of code in your client and your host to
allow an intermediate server to balance the incoming client loads.

This means the steps followed to get a connection underway would be:

  1. Server awaits incoming client connections at application URL. (i.e. `server.myapp.com`)
  2. Client connects to Server, and requests to be forwarded to a real host.
  3. Server decides what host is best fit to handle a connection right now, and it
     returns a new endpoint to the client. (i.e. `some.host.ip.address`)
  4. Client continues its regular discussion with the backend with new endpoint.

### Table of Contents

  - [Establishing Hosts](#Establishing%20Hosts)
  - [Defining the Server](#Defining%20the%20Server)
  - [Creating Clients](#Creating%20Clients)

## Establishing Hosts

```javascript
// load up balancr, and define yourself as a host,
// and specify the server that will balance for you
var balancr = require('balancr')('host', 'server.myapp.com'),

    // write up a server the way you normally would,
    // port is irrelevant and you can handle it without
    // any extra interaction or prelimary processing
    http = require('http').Server(function (req, res) {
      balancr.push()

      setTimeout(function () {
        res.end('Hello, ' + req.body)
        balancr.pop()
      }, 2500)
    })

// start your server like usual
http.listen(80)
```

*balancr.update() will update the load size with the current CPU usage if no argument is provided, or with a custom numeric value if provided.*

## Defining the Server

```javascript
// define yourself as the server, and
// identify the hosts behind you
var balancr = require('balancr')('server', [
  'host.ip.addr.1',
  'host.ip.addr.2',
  'host.ip.addr.3'
])
```

## Creating Clients

```javascript
// this will return a string with the host ip address
// (therefore it is fully synchronous; sorry)
var request = require('request')

require('balancr')('client', 'server.myapp.com', function (host) {

  // connect the way you usually would
  request('http://' + host + '/', function (err, res, data) {
    // ...
  })

})
    
```

## License

Under [the GPLv3](https://github.com/OHDB/balancr).
