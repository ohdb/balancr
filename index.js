/**
 * index.js - balancr
 * Licensed under GPLv3.
 * Copyright (C) 2015 Online Health Database.
 **/

"use strict";

module.exports = function (role) {
    // grab module based on role
    var construct = require('./lib/' + role),
        args = Array.prototype.slice.call(arguments, 1)

    // call with remaining arguments
    return construct.apply(this, args)
}